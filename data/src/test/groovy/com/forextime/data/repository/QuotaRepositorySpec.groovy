package com.forextime.data.repository

import com.forextime.base_unit_test.RxJavaRule
import com.forextime.data.socket.QuotaSocketHelper
import com.forextime.data.socket.QuotaSocketStateHolder
import com.forextime.domain.model.*
import io.reactivex.Observable
import kotlin.Pair
import org.junit.ClassRule
import spock.lang.Shared
import spock.lang.Specification

class QuotaRepositorySpec extends Specification {

    @ClassRule
    @Shared
    RxJavaRule rxJavaRule = new RxJavaRule()

    QuotaSocketHelper socketHelper = Mock()
    QuotaSocketStateHolder socketStateHolder = Mock()
    def repository = new QuotaRepositoryImpl(
            socketHelper,
            socketStateHolder
    )

    def 'should return init result'() {
        given:
        def expectedQuotas = [
                new Pair<Currency, Currency>(Currency.BTC, Currency.USD)
        ]
        and:
        socketStateHolder.stateObserver >> Observable.empty()
        when:
        def testObserver = repository.subscribeToQuotas(expectedQuotas).test()
        then:
        1 * socketStateHolder.setQuotas(expectedQuotas)
        1 * socketHelper.connect()
        1 * socketHelper.close()
        testObserver.assertResult(Connection.INSTANCE)
    }

    def 'should reconnect'() {
        given:
        socketStateHolder.stateObserver >> Observable.just(Failure.INSTANCE)
        when:
        def testObserver = repository.subscribeToQuotas([]).test()
        then:
        2 * socketHelper.connect()
        1 * socketHelper.close()
        testObserver.assertResult(Connection.INSTANCE, Connection.INSTANCE)
    }

    def 'should return connected and item'() {
        given:
        def quotaItem = new QuotaItem("same", 1G)
        socketStateHolder.stateObserver >> Observable.just(Connected.INSTANCE, quotaItem)
        when:
        def testObserver = repository.subscribeToQuotas([]).test()
        then:
        1 * socketHelper.connect()
        1 * socketHelper.close()
        testObserver.assertResult(Connection.INSTANCE, Connected.INSTANCE, quotaItem)
    }
}
