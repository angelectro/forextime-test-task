package com.forextime.data.mapper

import com.forextime.data.entity.EventType
import com.forextime.data.entity.QuotaEventEntity
import com.forextime.domain.model.QuotaItem
import spock.lang.Specification

class QuotaItemMapperSpec extends Specification {

    def mapper = new QuotaItemMapper()

    def 'should map correctly'() {
        given:
        def source = new QuotaEventEntity(100G, "BTC-USD", EventType.TICKER)
        def expectedResult = new QuotaItem("BTC-USD", 100G)
        when:
        def actualResult = mapper.map(source)
        then:
        actualResult == expectedResult
    }
}
