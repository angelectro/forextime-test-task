package com.forextime.data.socket

interface QuotaSocketHelper {

    fun connect()

    fun close()
}