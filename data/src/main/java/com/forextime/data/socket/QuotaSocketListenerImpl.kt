package com.forextime.data.socket

import android.util.Log
import com.forextime.data.entity.Channel
import com.forextime.data.entity.EventType
import com.forextime.data.entity.QuotaEventEntity
import com.forextime.data.entity.SubscribeRequest
import com.forextime.data.mapper.Mapper
import com.forextime.domain.model.*
import com.google.gson.Gson
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import okhttp3.Response
import okhttp3.WebSocket
import okhttp3.WebSocketListener
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class QuotaSocketListenerImpl @Inject constructor(
    private val gson: Gson,
    private val mapper: @JvmSuppressWildcards Mapper<QuotaEventEntity, QuotaItem>
) : WebSocketListener(), QuotaSocketStateHolder {

    override var quotas: List<Pair<Currency, Currency>>? = null

    private val stateSubject = BehaviorSubject.create<StateAction>()

    override val stateObserver: Observable<StateAction>
        get() = stateSubject

    override fun onOpen(webSocket: WebSocket, response: Response) {
        stateSubject.onNext(Connected)
        quotas?.let { subscribe(webSocket, it) }
    }

    override fun onMessage(webSocket: WebSocket, text: String) {
        Log.d(this::class.java.simpleName, text)
        processMessage(text)
    }

    override fun onFailure(webSocket: WebSocket, t: Throwable, response: Response?) {
        stateSubject.onNext(Failure)
    }

    private fun subscribe(
        webSocket: WebSocket,
        list: List<Pair<Currency, Currency>>
    ) {
        val quotasStrings = list.map { "${it.first.name}-${it.second.name}" }
        val channels = listOf(Channel(quotasStrings))
        val requestText = gson.toJson(SubscribeRequest(quotasStrings, channels))
        webSocket.send(requestText)
    }

    private fun processMessage(text: String) {
        val entity = gson.fromJson(text, QuotaEventEntity::class.java)
        if (entity.type == EventType.TICKER) {
            stateSubject.onNext(mapper.map(entity))
        }
    }
}