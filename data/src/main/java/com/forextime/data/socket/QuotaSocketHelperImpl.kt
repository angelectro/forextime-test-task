package com.forextime.data.socket

import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.WebSocket
import okhttp3.WebSocketListener
import javax.inject.Inject

class QuotaSocketHelperImpl @Inject constructor(
    private val client: OkHttpClient,
    private val listener: WebSocketListener
) : QuotaSocketHelper {

    companion object {
        private const val SOCKET_URL = "wss://ws-feed.pro.coinbase.com"
        const val CLOSE_CODE = 555
    }

    private var webSocket: WebSocket? = null

    override fun connect() {
        val request = Request.Builder().url(SOCKET_URL).build()
        webSocket = client.newWebSocket(request, listener)
    }

    override fun close() {
        webSocket?.close(CLOSE_CODE, null)
    }
}