package com.forextime.data.socket

import com.forextime.domain.model.Currency
import com.forextime.domain.model.StateAction
import io.reactivex.Observable

interface QuotaSocketStateHolder {

    val stateObserver: Observable<StateAction>

    var quotas: List<Pair<Currency, Currency>>?
}