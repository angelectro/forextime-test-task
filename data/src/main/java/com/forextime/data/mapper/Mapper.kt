package com.forextime.data.mapper

interface Mapper<SOURCE, TARGET> {

    fun map(source: SOURCE): TARGET
}