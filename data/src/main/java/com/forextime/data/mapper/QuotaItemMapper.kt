package com.forextime.data.mapper

import com.forextime.data.entity.QuotaEventEntity
import com.forextime.domain.model.QuotaItem
import javax.inject.Inject

class QuotaItemMapper
@Inject constructor() : Mapper<@JvmSuppressWildcards QuotaEventEntity, @JvmSuppressWildcards QuotaItem> {

    override fun map(source: QuotaEventEntity) =
        with(source) {
            QuotaItem(
                productId,
                price
            )
        }
}