package com.forextime.data.repository

import com.forextime.data.socket.QuotaSocketHelper
import com.forextime.data.socket.QuotaSocketStateHolder
import com.forextime.domain.model.Connection
import com.forextime.domain.model.Currency
import com.forextime.domain.model.Failure
import com.forextime.domain.model.StateAction
import com.forextime.domain.repository.QuotaRepository
import io.reactivex.Observable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

private const val CONNECTION_DELAY = 3000L

class QuotaRepositoryImpl
@Inject constructor(
    private val socketHelper: QuotaSocketHelper,
    private val socketListener: QuotaSocketStateHolder
) : QuotaRepository {

    override fun subscribeToQuotas(quotas: List<Pair<Currency, Currency>>): Observable<StateAction> {
        socketListener.quotas = quotas
        return socketListener.stateObserver
            .startWith(Connection)
            .doOnSubscribe { socketHelper.connect() }
            .doOnTerminate { socketHelper.close() }
            .flatMap { state ->
                if (state is Failure) {
                    Observable.timer(CONNECTION_DELAY, TimeUnit.MILLISECONDS)
                        .doOnNext { socketHelper.connect() }
                        .map { state }
                } else {
                    Observable.just(state)
                }
            }.map {
                when (it) {
                    Failure -> Connection
                    else -> it
                }
            }
    }
}