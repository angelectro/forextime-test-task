package com.forextime.data.entity

import com.google.gson.annotations.SerializedName

private const val TICKER_CHANNEL = "ticker"
private const val TYPE_SUBSCRIBE = "subscribe"

data class SubscribeRequest(
    @SerializedName("product_ids")
    val productIds: List<String>,
    @SerializedName("channels")
    val channels: List<Channel>,
    @SerializedName("type")
    val type: String = TYPE_SUBSCRIBE
)

data class Channel(
    @SerializedName("product_ids")
    val productIds: List<String>,
    @SerializedName("name")
    val name: String = TICKER_CHANNEL
)