package com.forextime.data.entity

import com.google.gson.annotations.SerializedName

enum class EventType {

    @SerializedName("ticker")
    TICKER,

    @SerializedName("subscriptions")
    SUBSCRIPTIONS
}