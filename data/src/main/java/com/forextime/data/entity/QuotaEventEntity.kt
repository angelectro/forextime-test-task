package com.forextime.data.entity

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

data class QuotaEventEntity(
    @SerializedName("price")
    val price: BigDecimal,
    @SerializedName("product_id")
    val productId: String,
    @SerializedName("type")
    val type: EventType
)