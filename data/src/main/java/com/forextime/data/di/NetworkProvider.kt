package com.forextime.data.di

import com.forextime.domain.repository.QuotaRepository
import com.google.gson.Gson

interface NetworkProvider {

    fun provideQuotaRepository(): QuotaRepository

    fun provideGson(): Gson
}