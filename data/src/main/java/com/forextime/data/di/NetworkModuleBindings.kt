package com.forextime.data.di

import com.forextime.data.entity.QuotaEventEntity
import com.forextime.data.mapper.Mapper
import com.forextime.data.mapper.QuotaItemMapper
import com.forextime.data.repository.QuotaRepositoryImpl
import com.forextime.data.socket.QuotaSocketHelper
import com.forextime.data.socket.QuotaSocketHelperImpl
import com.forextime.data.socket.QuotaSocketListenerImpl
import com.forextime.data.socket.QuotaSocketStateHolder
import com.forextime.domain.model.QuotaItem
import com.forextime.domain.repository.QuotaRepository
import dagger.Binds
import dagger.Module
import okhttp3.WebSocketListener
import javax.inject.Singleton

@Module
interface NetworkModuleBindings {

    @Binds
    @Singleton
    fun bindsSocketHelper(socketHelper: QuotaSocketHelperImpl): QuotaSocketHelper

    @Binds
    @Singleton
    fun bindsSocketStateHolder(socketHelper: QuotaSocketListenerImpl): QuotaSocketStateHolder

    @Binds
    @Singleton
    fun bindsSocketListener(socketHelper: QuotaSocketListenerImpl): WebSocketListener

    @Binds
    @Singleton
    fun bindsRepository(repository: QuotaRepositoryImpl): QuotaRepository

    @Binds
    @Singleton
    fun provideQuotaActionMapper(mapper: QuotaItemMapper): Mapper<QuotaEventEntity, QuotaItem>
}