package com.forextime.testtask.feature

import com.forextime.base_unit_test.RxJavaRule
import com.forextime.domain.interactor.QuotaInteractor
import com.forextime.domain.model.Connected
import com.forextime.domain.model.Connection
import com.forextime.domain.model.Failure
import com.forextime.domain.model.QuotaItem
import io.reactivex.Observable
import org.junit.ClassRule
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

class QuotaViewModelSpec extends Specification {

    @ClassRule
    @Shared
    RxJavaRule rxJavaRule

    QuotaInteractor interactor = Mock()
    def viewModel = new QuotaViewModel(interactor)

    @Unroll
    def 'should return view state correctly'() {
        given:
        def testObserver = viewModel.states().test()
        when:
        viewModel.processIntents(Observable.just(QuotaIntent.InitialIntent.INSTANCE))
        then:
        1 * interactor.subscribeToQuotas(*_) >> Observable.just(state)
        testObserver.assertResult(expectedViewState)
        where:
        state                     | expectedViewState
        Connection.INSTANCE       | new QuotaViewState(true, [])
        Failure.INSTANCE          | new QuotaViewState(true, [])
        Connected.INSTANCE        | new QuotaViewState(false, [])
        new QuotaItem("same", 4G) | new QuotaViewState(false, [new QuotaItem("same", 4G)])
    }
}
