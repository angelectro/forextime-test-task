package com.forextime.testtask.di.quota

import androidx.lifecycle.ViewModel
import com.forextime.domain.interactor.QuotaInteractor
import com.forextime.domain.interactor.QuotaInteractorImpl
import com.forextime.testtask.di.FeatureScope
import com.forextime.testtask.di.viewmodel.ViewModelKey
import com.forextime.testtask.feature.QuotaViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module(includes = [QuotaModule.QuotaBindings::class])
class QuotaModule {

    @Module
    abstract class QuotaBindings {

        @Binds
        @IntoMap
        @FeatureScope
        @ViewModelKey(QuotaViewModel::class)
        abstract fun provideViewModel(viewModel: QuotaViewModel): ViewModel

        @Binds
        @FeatureScope
        abstract fun ptovideIntercator(interactor: QuotaInteractorImpl): QuotaInteractor
    }

}