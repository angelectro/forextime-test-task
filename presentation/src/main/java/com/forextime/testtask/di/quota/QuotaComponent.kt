package com.forextime.testtask.di.quota

import com.forextime.testtask.di.FeatureScope
import com.forextime.testtask.di.app.AppProvider
import com.forextime.testtask.di.viewmodel.ViewModelModule
import com.forextime.testtask.feature.QuotaActivity
import dagger.Component

@FeatureScope
@Component(
    modules = [QuotaModule::class, ViewModelModule::class],
    dependencies = [AppProvider::class]
)
interface QuotaComponent {
    companion object {

        fun create(appProvider: AppProvider): QuotaComponent {
            return DaggerQuotaComponent
                .builder()
                .appProvider(appProvider)
                .build()
        }
    }

    fun inject(activity: QuotaActivity)
}