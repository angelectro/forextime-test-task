package com.forextime.testtask.di.app

import android.app.Application
import com.forextime.data.di.NetworkModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [AppModule::class, NetworkModule::class]
)
interface AppComponent : AppProvider {

    companion object {

        fun create(application: Application): AppComponent {
            return DaggerAppComponent
                .builder()
                .appModule(AppModule(application))
                .build()
        }
    }
}