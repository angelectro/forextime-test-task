package com.forextime.testtask.di.app

interface AbstractApp {

    fun getApplicationProvider(): AppProvider
}