package com.forextime.testtask.di

@javax.inject.Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class FeatureScope
