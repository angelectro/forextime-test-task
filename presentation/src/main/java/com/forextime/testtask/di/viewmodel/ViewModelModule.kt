package com.forextime.testtask.di.viewmodel

import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module

@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindsFactory(viewModelFactory: ViewModelFactoryProvider): ViewModelProvider.Factory
}