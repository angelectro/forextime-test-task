package com.forextime.testtask.di.app

import android.app.Application
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class AppModule
constructor(
    private val application: Application
) {

    @Provides
    @Singleton
    fun provideContext() = application.applicationContext
}