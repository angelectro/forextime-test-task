package com.forextime.testtask

import android.app.Application
import com.forextime.testtask.di.app.AbstractApp
import com.forextime.testtask.di.app.AppComponent
import com.forextime.testtask.di.app.AppProvider

class App : Application(), AbstractApp {

    companion object {

        private var appComponent: AppComponent? = null
    }

    override fun onCreate() {
        super.onCreate()
        appComponent = AppComponent.create(this)
    }

    override fun getApplicationProvider(): AppProvider = appComponent!!
}