package com.forextime.testtask.feature

import androidx.lifecycle.ViewModel
import com.forextime.domain.interactor.QuotaInteractor
import com.forextime.domain.model.*
import com.forextime.testtask.base.BaseViewModel
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit
import javax.inject.Inject

private const val TIMEOUT = 500L

class QuotaViewModel
@Inject constructor(
    private val quotaInteractor: QuotaInteractor
) : ViewModel(), BaseViewModel<QuotaIntent, QuotaViewState> {

    private var intentsSubject = PublishSubject.create<QuotaIntent>()
    private val intentFilter = ObservableTransformer<QuotaIntent, QuotaIntent> {
        it.publish {
            Observable.merge(it.ofType(QuotaIntent.InitialIntent::class.java).take(1),
                it.filter { intent -> intent !is QuotaIntent.InitialIntent })
        }
    }
    private val statesSubject: Observable<QuotaViewState> = intentsSubject.compose(intentFilter)
        .flatMap { actionFromIntent(it) }
        .replay(1)
        .autoConnect(0)

    private val items = mutableListOf<QuotaItem>()
    private val quotas = listOf(Currency.BTC to Currency.USD)

    override fun processIntents(intents: Observable<QuotaIntent>) {
        intents.subscribe(intentsSubject)
    }

    override fun states(): Observable<QuotaViewState> {
        return statesSubject
    }

    private fun actionFromIntent(intent: QuotaIntent): Observable<QuotaViewState> {
        return when (intent) {
            is QuotaIntent.InitialIntent -> getQuotas()
        }
    }

    private fun getQuotas(): Observable<QuotaViewState> {
        return quotaInteractor.subscribeToQuotas(quotas)
            .subscribeOn(Schedulers.io())
            .debounce(TIMEOUT, TimeUnit.MILLISECONDS)
            .map(::processAction)
    }

    private fun processAction(state: StateAction): QuotaViewState {
        return when (state) {
            Connection, Failure -> QuotaViewState(true, items)
            Connected -> QuotaViewState(false, items)
            is QuotaItem -> {
                items.add(state)
                QuotaViewState(false, items)
            }
        }
    }
}