package com.forextime.testtask.feature

import com.forextime.domain.model.QuotaItem
import com.forextime.testtask.base.BaseViewState

data class QuotaViewState(
    val isConnection: Boolean,
    val items: List<QuotaItem>
) : BaseViewState