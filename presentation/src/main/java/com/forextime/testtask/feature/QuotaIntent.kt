package com.forextime.testtask.feature

import com.forextime.testtask.base.BaseIntent

sealed class QuotaIntent : BaseIntent {

    object InitialIntent : QuotaIntent()
}