package com.forextime.testtask.feature

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.forextime.domain.model.QuotaItem
import com.forextime.testtask.R
import com.forextime.testtask.base.BaseView
import com.forextime.testtask.di.app.AbstractApp
import com.forextime.testtask.di.quota.QuotaComponent
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.ValueFormatter
import com.google.android.material.snackbar.Snackbar
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_main.*
import java.math.BigDecimal
import javax.inject.Inject


class QuotaActivity : AppCompatActivity(), BaseView<QuotaIntent, QuotaViewState> {

    @Inject
    lateinit var factory: ViewModelProvider.Factory
    private lateinit var viewModel: QuotaViewModel
    private val compositeDisposable = CompositeDisposable()
    private var connectionSnackbar: Snackbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        inject()
        bindViewModel()
        initChart()
    }

    private fun inject() {
        val appProvider = (application as AbstractApp).getApplicationProvider()
        QuotaComponent.create(appProvider).inject(this)
    }

    private fun bindViewModel() {
        viewModel = ViewModelProviders.of(this, factory).get(QuotaViewModel::class.java)
        compositeDisposable.add(
            viewModel.states()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(::render)
        )
        viewModel.processIntents(intents())
    }

    override fun intents(): Observable<QuotaIntent> = Observable.just(QuotaIntent.InitialIntent)

    private fun initChart() {
        with(quotaChart) {
            isDragEnabled = true
            isAutoScaleMinMaxEnabled = true
            setScaleEnabled(true)
            setMaxVisibleValueCount(15)
            description.isEnabled = false
            axisLeft.isEnabled = false
            xAxis.position = XAxis.XAxisPosition.BOTTOM
        }
    }

    override fun render(state: QuotaViewState) {
        populateItems(state.items)

        if (state.isConnection) {
            showConnection()
        } else {
            hideConnection()
        }

    }

    private fun populateItems(items: List<QuotaItem>) {
        val entries = createEntries(items)
        val dataSet = createDataSet(items, entries)

        with(quotaChart) {
            data = LineData(dataSet)
            notifyDataSetChanged()
            invalidate()
        }
    }

    private fun createEntries(items: List<QuotaItem>): List<Entry> {
        return items.mapIndexed { index, quotaItem ->
            val price = quotaItem.price.setScale(2, BigDecimal.ROUND_HALF_UP).toFloat()
            Entry(index.toFloat(), price)
        }
    }

    private fun createDataSet(items: List<QuotaItem>, entries: List<Entry>): LineDataSet {
        val label = items.map { it.productId }.firstOrNull() ?: ""
        return LineDataSet(entries, label).apply {
            mode = LineDataSet.Mode.CUBIC_BEZIER
            fillColor = ContextCompat.getColor(applicationContext, R.color.chartFill)
            color = ContextCompat.getColor(applicationContext, R.color.chartLine)
            setDrawFilled(true)
            valueFormatter = object : ValueFormatter() {
                override fun getFormattedValue(value: Float): String {
                    return value.toBigDecimal().setScale(2).toString()
                }
            }
        }
    }

    private fun showConnection() {
        if (connectionSnackbar?.isShown?.not() != false) {
            connectionSnackbar = Snackbar.make(mainRoot, getString(R.string.connection), Snackbar.LENGTH_INDEFINITE)
            connectionSnackbar?.show()
        }
    }

    private fun hideConnection() {
        connectionSnackbar?.dismiss()
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }
}
