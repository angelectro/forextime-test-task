package com.forextime.base_unit_test

import io.reactivex.Scheduler
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.disposables.Disposable
import io.reactivex.internal.schedulers.ExecutorScheduler
import io.reactivex.plugins.RxJavaPlugins
import org.junit.rules.ExternalResource

import java.util.concurrent.Executor
import java.util.concurrent.TimeUnit

class RxJavaRule extends ExternalResource {

    @Override
    protected void before() throws Throwable {
        replaceSchedulers()
    }

    protected void replaceSchedulers() {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { createTestSchedulers() }
        RxJavaPlugins.setComputationSchedulerHandler { createTestSchedulers() }
        RxJavaPlugins.setIoSchedulerHandler { createTestSchedulers() }
        RxJavaPlugins.setInitNewThreadSchedulerHandler { createTestSchedulers() }
        RxJavaPlugins.setSingleSchedulerHandler { createTestSchedulers() }
    }

    private static createTestSchedulers() {
        return new Scheduler() {

            @Override
            Disposable scheduleDirect(Runnable run, long delay, TimeUnit unit) {
                // this prevents StackOverflowErrors when scheduling with a delay
                return super.scheduleDirect(run, 0, unit)
            }

            @Override
            ExecutorScheduler.ExecutorWorker createWorker() {
                return new ExecutorScheduler.ExecutorWorker(new Executor() {

                    void execute(Runnable command) {
                        command.run()
                    }
                }, false)
            }
        }
    }

    @Override
    protected void after() {
        RxJavaPlugins.reset()
        RxAndroidPlugins.reset()
    }
}