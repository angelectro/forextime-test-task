package com.forextime.domain.interactor

import com.forextime.domain.model.Connected
import com.forextime.domain.model.Currency
import com.forextime.domain.repository.QuotaRepository
import io.reactivex.Observable
import kotlin.Pair
import spock.lang.Specification

class QuotaInteractorSpec extends Specification {

    QuotaRepository repository = Mock()
    QuotaInteractorImpl interactor = new QuotaInteractorImpl(repository)

    def 'should return result correctly'() {
        given:
        def expectedQuotas = [
                new Pair<Currency, Currency>(Currency.BTC, Currency.USD)
        ]
        def expectedResult = Connected.INSTANCE
        when:
        def testObserver = interactor.subscribeToQuotas(expectedQuotas).test()
        then:
        1 * repository.subscribeToQuotas(_) >> { args ->
            assert args[0] == expectedQuotas
            return Observable.just(expectedResult)
        }
        testObserver.assertResult(expectedResult)
    }

}
