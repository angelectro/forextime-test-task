package com.forextime.domain.model

enum class Currency {
    RUB, USD, EUR, ETH, BTC
}