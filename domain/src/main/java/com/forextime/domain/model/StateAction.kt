package com.forextime.domain.model

import java.math.BigDecimal

sealed class StateAction

object Connection : StateAction()

object Connected : StateAction()

object Failure : StateAction()

data class QuotaItem(
    val productId: String,
    val price: BigDecimal
) : StateAction()