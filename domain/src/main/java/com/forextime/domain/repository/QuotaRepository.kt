package com.forextime.domain.repository

import com.forextime.domain.model.Currency
import com.forextime.domain.model.StateAction
import io.reactivex.Observable

interface QuotaRepository {

    fun subscribeToQuotas(quotas: List<Pair<Currency, Currency>>): Observable<StateAction>
}