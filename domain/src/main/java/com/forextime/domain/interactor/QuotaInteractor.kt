package com.forextime.domain.interactor

import com.forextime.domain.model.Currency
import com.forextime.domain.model.StateAction
import io.reactivex.Observable

interface QuotaInteractor {

    fun subscribeToQuotas(quotas: List<Pair<Currency, Currency>>): Observable<StateAction>
}