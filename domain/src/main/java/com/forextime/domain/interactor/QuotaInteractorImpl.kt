package com.forextime.domain.interactor

import com.forextime.domain.model.Currency
import com.forextime.domain.model.StateAction
import com.forextime.domain.repository.QuotaRepository
import io.reactivex.Observable
import javax.inject.Inject

class QuotaInteractorImpl
@Inject constructor(
    private val quotaRepository: QuotaRepository
) : QuotaInteractor {

    override fun subscribeToQuotas(quotas: List<Pair<Currency, Currency>>): Observable<StateAction> {
        return quotaRepository.subscribeToQuotas(quotas)
    }
}